<?php

namespace App\Http\Controllers;

// use App\DataTables\CompaniesDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use DataTables;
use App\Models\Companies;
use App\Models\Employees;
// use App\DataTables\CompaniesDataTable;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'email|max:255|nullable',
            'logo' => 'image|file|mimes:jpeg,jpg,png,gif|max:2048|nullable',
            'url' => 'nullable'
        ]);

        if ($validator->fails()) {
            
            return redirect()->route('companies.create')->withErrors($validator);

        }

        $company = Companies::create($request->all());

        if($request->hasFile('logo')) {
            $file = $request->file('logo');
 
            //you also need to keep file extension as well
            $name = $request->name.'.'.$file->getClientOriginalExtension();
 
            //using the array instead of object
            $image['filePath'] = $name;
            $file->move(storage_path().'/', $name);
            $company->logo= storage_path().'/'. $name;
            $company->save();
         }

        return redirect()->route('companies.index')->with(['message'=>'saved']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if($request->ajax()){
            $company_data = Companies::find($id);
            $employees_data = Employees::where('company_id', $id)->pluck('company_id');

            if(empty($employees_data)){
                return response(false);
            }else{
                $company_data->delete();
                return response(true);
            }

        }
    }

    public function get_data_json(Request $request){
        if(!Auth::check()){return redirect()->url('/');}

        if($request->ajax()){

            $data = Companies::get();

            return DataTables::of($data) 
                    ->addColumn('action', function($row){
                        $btn = '<a href= companies/'.$row->id.'/edit class="edit btn btn-warning btn-sm editCompany" type="button">
                                 <span class="fa fa-edit" style="color:white;"></span></a>
                                 <button class="btn btn-danger btn-sm delete" data-id="'.$row->id.'">
                                    <span class="fa fa-trash"></span>
                                 </button>';
                         return $btn;
                    })
                    ->addIndexColumn()  
                    ->setRowId('id')
                    ->rawColumns(['action'])
                    ->make(true);
        }

    }

}
