<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use DataTables;

use App\Models\Employees;
use App\Models\Companies;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_company = Companies::get();
        return view('employees.index', compact('data_company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'email'
        ]);

        if ($validator->fails()) {
            
            return response()->json(['errors' => $validator->errors()]);

        }

        Employees::create([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "phone" => $request->phone,
            "company_id" => $request->company_id
        ]);
        
        return response()->json(['success' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id){
        if($request->ajax()){
            $data = Employees::find($id);
            return response($data);
        }
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'email'
        ]);

        if ($validator->fails()) {
            
            return response()->json(['errors' => $validator->errors()]);

        }

        Employees::find($id)->update([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "phone" => $request->phone,
            "company_id" => $request->company_id
        ]);
        
        return response()->json(['success' => '1']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id){
        if($request->ajax()){
            $data = Employees::find($id);
            if(empty($data)){
                return response(false);
            }else{
                $data->delete();
                return response(true);
            }

        }
    }

    public function get_data_json(Request $request){
        if(!Auth::check()){return redirect()->url('/');}

        if($request->ajax()){

            $data = Employees::get();

            return DataTables::of($data) 
                    ->editColumn("fullname", function ($data) {
                        return $data->first_name.' '.$data->last_name;
                    })                   
                    ->addColumn('action', function($row){
                        $btn = '<button class="btn btn-warning btn-sm edit" data-id="'.$row->id.'" title="Edit">
                                    <i class="fa fa-pencil" style="color:white;"></i>
                                </button>
                                <button class="btn btn-danger btn-sm delete" data-id="'.$row->id.'" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </button>';
                         return $btn;
                    })
                    ->addIndexColumn()
                    ->setRowId('id')
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
    }
}
