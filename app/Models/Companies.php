<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $table = 'companies';

    protected $searchableColumns = ['name'];

    protected $fillable = ['name', 'email', 'logo', 'website', 'created_at', 'updated_at'];
}
