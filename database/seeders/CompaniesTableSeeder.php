<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i=0; $i<=15; $i++){
            $data[$i] = [
                'name' => $faker->name,
                'email' => $faker->email,
                'logo' => $faker->imageUrl($width = 640, $height = 480),
                'website' => $faker->url,
            ];
        }

        $insert = DB::table('companies')->insert($data);
    }
}
