<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Companies;
use Faker\Factory as Faker;
use Faker\Provider\Lorem;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $company_id = Companies::pluck('id')->toArray();

        for ($i=0; $i<=15; $i++){
            $data[$i] = [
                'first_name' => $faker->firstNameMale,
                'last_name' => $faker->lastName,
                'company_id' => $faker->numberBetween(1,15),
                'email' => $faker->email,
                'phone' => $faker->e164PhoneNumber ,
            ];
        }

        $insert = DB::table('employees')->insert($data);


    }
}
