<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user[0] = [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
        ];
        $user[1] = [
            'name' => 'User',
            'email' => 'user@user.com',
            'password' => Hash::make('password'),
        ];

        DB::table('users')->insert($user);
    }
}
