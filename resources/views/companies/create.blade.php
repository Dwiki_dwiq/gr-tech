@extends('layouts.app')
@section('pageTitle', 'Create Companies')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header bg-primary"> 
                        <h6 style="color:white;">Create New Company</h6>
                    </div>
                    <div class="card-body">
                        <form id="formCreateCompany" action="{{route('companies.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group has-feedback row">
                                <label for="" class="col-3">
                                    Company Name
                                </label>
                                <div class="col-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-building" style="width:20px;"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Type Company Name">
                                    </div>
                                    @error('name')
                                    <span class="text-danger">
                                        <strong id="company-name-error">{{$message}}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>                      
                            <div class="form-group has-feedback row">
                                <label for="" class="col-3">
                                    Email
                                </label>
                                <div class="col-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-envelope" style="width:20px;"></i>
                                            </span>
                                        </div>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Type Email">
                                    </div>
                                    @error('email')
                                    <span class="text-danger">
                                        <strong id="email-error">{{message}}</strong>
                                    </span>
                                    @enderror
                                </div>                       
                            </div>
                            <div class="form-group has-feedback row">
                                <label for="" class="col-3">
                                    Logo
                                    <br>
                                    <span style="color:red;">*</span> <small><i>max-size:2MB</i></small>
                                </label>
                                <div class="col-6">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="logo" id="logo">
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                    </div>
                                    @error('logo')
                                    <span class="text-danger">
                                        <strong id="logo-error">{{$errors->message}}</strong>
                                    </span>
                                    @enderror
                                </div>                            
                            </div>
                            <div class="form-group has-feedack row">
                                <label for="" class="col-3">
                                    Website
                                </label>
                                <div class="col-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-link" style="width:20px;"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" name="url" id="url" placeholder="Type Website URL">
                                    </div>
                                    @error('url')
                                    <span class="text-danger">
                                        <strong id="link-error">{{$message}}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <div class="row justify-content-end">
                                <div class="col-md-9">
                                    <button type="button" class="btn btn-outline-info" onclick="window.history.back()">
                                        <span><i class="fa fa-long-arrow-left"></i> Back</span>
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        <span><i class="fa fa-save"></i> Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                   
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(function(){
        $('#logo').on('change', function(){ 
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings("#formCreateCompany .custom-file-label").addClass("selected").html(fileName);   
        });
    });
</script>
@stop