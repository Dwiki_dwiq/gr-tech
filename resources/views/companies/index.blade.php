@extends('layouts.app')
@section('pageTitle', 'Companies')
@section('content')

@if (session('success'))
    <script>
        $( document ).ready(function{
            swal.fire("{{ session('success') }}")
        });
    </script>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary float-right mb-3" id="create-company" href="{{route('companies.create')}}">
                <i class="fa fa-plus"></i> <span>New Company</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="companies-table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <!-- <th>Created At</th>
                        <th>Updated At</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
    
@endsection

@section('modal')
<!-- <div class="modal" tabindex="-1" role="dialog" id="company-create-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formCreateCompany" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group has-feedback row">
                            <label for="" class="col-3">
                                Company Name
                            </label>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-building" style="width:20px;"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Type Company Name" required>
                                </div>
                                <span class="text-danger">
                                    <strong id="company-name-error"></strong>
                                </span>
                            </div>
                        </div>                      
                        <div class="form-group has-feedback row">
                            <label for="" class="col-3">
                                Email
                            </label>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-envelope" style="width:20px;"></i>
                                        </span>
                                    </div>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Type Email">
                                </div>
                                <span class="text-danger">
                                    <strong id="email-error"></strong>
                                </span>
                            </div>                       
                        </div>
                        <div class="form-group has-feedback row">
                            <label for="" class="col-3">
                                Logo
                                <br>
                                <span style="color:red;">*</span> <small><i>max-size:2MB</i></small>
                            </label>
                            <div class="col-6">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input form-control" name="logo" id="logo">
                                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                                </div>
                                <span class="text-danger">
                                    <strong id="logo-error"></strong>
                                </span>
                            </div>                            
                        </div>
                        <div class="form-group has-feedack row">
                            <label for="" class="col-3">
                                Website
                            </label>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-link" style="width:20px;"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="url" id="url" placeholder="Type Website URL">
                                </div>
                                <span class="text-danger">
                                    <strong id="link-error"></strong>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> -->
@endsection

@section('scripts')
<script>
    $(function() {
        var datatable = $('#companies-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("companies.getdata")}}',
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                // { data: 'created_at', name: 'created_at' },
                // { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#companies-table tbody').on('click', '.delete', function (e) {
            xid = $(this).data('id');
            
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!"
            }).then ((result) => {
                if(result.isConfirmed){
                    $.ajax({
                        type : "DELETE",
                        url : '{{url("companies/")}}/'+xid,
                        data : { id: xid },
                        success : function(response){
                            if(response == 1){
                                swal.fire("Done!", "It was succesfully deleted!", "success");
                            }else if(response == 0){
                                swal.fire("Nothing to do!", "Data has relation!", "error");
                            }
                            datatable.ajax.reload();
                        },
                        error : function(response){
                            swal.fire("Error deleting!", "Please try again", "error");
                        }
                    });
                }
            });                
        });

       
    });
</script>
@stop