@extends('layouts.app')
@section('pageTitle', 'Employees')
@section('navTitle', 'Data Employees')
@section('content') 
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary mb-3 float-right" id="createNewEmployee">
                <i class="fa fa-plus"></i>
                <span> New Employee</span>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="employees-table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

    
@endsection

@section('modal')
    <div class="modal" tabindex="-1" role="dialog" id="employee-create-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCreateEmployee" method="POST">
                    @csrf
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            First Name
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Type First Name">
                            </div>
                            <span class="text-danger">
                                <strong id="first-name-error"></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            Last Name
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Type Last Name">                                
                            </div>
                            <span class="text-danger">
                                <strong id="last-name-error"></strong>
                            </span>
                        </div>
                    </div>                        
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            Email
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-envelope" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Type Email">
                            </div>
                            <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                        </div>                       
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-3">
                            Phone
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-phone" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Type Phone Number">
                            </div>
                        </div>
                        <span class="text-danger">
                            <strong id="phone-error"></strong>
                        </span>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-3">
                            Company
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-university" style="width:20px;"></i>
                                    </span>
                                </div>
                                <select class="form-control select-company" name="company_id" id="company_id" data-width="87%">
                                    @foreach($data_company as $data)
                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="employee-edit-modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Detail Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="formEditEmployee">
                    @csrf
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            First Name
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="first_name" id="first_name">
                            </div>
                            <span class="text-danger">
                                <strong id="first-name-error"></strong>
                            </span>
                        </div>
                    </div>
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            Last Name
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="last_name" id="last_name">
                            </div>
                            <span class="text-danger">
                                <strong id="last-name-error"></strong>
                            </span>
                        </div>
                    </div>                        
                    <div class="form-group has-feedback row">
                        <label for="" class="col-3">
                            Email
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-envelope" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                            <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-3">
                            Phone
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-phone" style="width:20px;"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="phone" id="phone">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-3">
                            Company
                        </label>
                        <div class="col-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-university" style="width:20px;"></i>
                                    </span>
                                </div>
                                <select name="company_id" id="company_id" data-width="87%" class="form-control select-company">
                                    @foreach($data_company as $data)
                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>                
                
            </div>
            <div class="modal-footer">
                <input type="text" id="employee_id" name="employee_id" hidden>
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function() {
            
            var datatable = $('#employees-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route("employees.getdata")}}',
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    { data: 'fullname', name: 'fullname' },
                    { data: 'email', name: 'email' },
                    // { data: 'created_at', name: 'created_at' },
                    // { data: 'updated_at', name: 'updated_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            
            $('#employees-table tbody').on('click', '.delete', function (e) {
                xid = $(this).data('id');
                
                swal.fire({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!"
                }).then ((result) => {
                    if(result.isConfirmed){
                        $.ajax({
                            type : "DELETE",
                            url : '{{url("employees/")}}/'+xid,
                            data : { id: xid },
                            success : function(response){
                                if(response == 1){
                                    swal.fire("Done!", "It was succesfully deleted!", "success");
                                }else if(response == 0){
                                    swal.fire("Nothing to do!", "Data is not found!", "info");
                                }
                                datatable.ajax.reload();
                            },
                            error : function(response){
                                swal.fire("Error deleting!", "Please try again", "error");
                            }
                        });
                    }
                });                
            });

            $('#employees-table tbody').on('click',".edit", function () {
                xid = $(this).data('id');        
                $.ajax({
                    type:"GET",
                    url: "{{url('employees')}}/"+ xid + "/edit",                
                    success:function(data){

                        $("#employee-edit-modal").find('#first_name').val(data.first_name);
                        $("#employee-edit-modal").find('#last_name').val(data.last_name);
                        $("#employee-edit-modal").find('#email').val(data.email);
                        $("#employee-edit-modal").find('#phone').val(data.phone);
                        $("#employee-edit-modal").find('#company_id').val(data.company_id).change();

                        $("#employee-edit-modal .modal-footer").find('#employee_id').val(xid);
                            
                        $("#employee-edit-modal").modal('show'); 
                    }
                });               
            });

            $('#employee-edit-modal .modal-footer').on('click',".btn-primary", function () {
                xid = $('#employee-edit-modal .modal-footer').find('#employee_id').val();
                
                var editEmployee = $("#formEditEmployee");
                var formData = editEmployee.serialize();

                editEmployee.find( '#first-name-error' ).html( "" );
                editEmployee.find( '#last-name-error' ).html( "" );
                editEmployee.find( '#email-error' ).html( "" );

                $.ajax({
                    url:'{{url("/employees")}}/'+xid,
                    type:'PATCH',
                    data:formData,
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.first_name){
                                editEmployee.find( '#first-name-error' ).html( data.errors.first_name[0] );
                            }
                            if(data.errors.last_name){
                                editEmployee.find( '#last-name-error' ).html( data.errors.last_name[0] );
                            }
                            if(data.errors.email){
                                editEmployee.find( '#email-error' ).html( data.errors.email[0] );
                            }
                        }
                        if(data.success) {
                            swal.fire('Done!','it was successfully Updated!', 'success');
                            $("#employee-edit-modal").modal("hide");
                            datatable.ajax.reload();
                        }
                    },
                });             
            });
            
            $("#createNewEmployee").on('click', function(e){
                $("#employee-create-modal").find('form').trigger('reset');
                
                $( '#first-name-error' ).html( "" );
                $( '#last-name-error' ).html( "" );
                $( '#email-error' ).html( "" );

                $("#employee-create-modal").modal("show");

                e.preventDefault();
            });

            $("#employee-create-modal .modal-footer").on('click', '.btn-primary', function(){

                var createEmployee = $("#formCreateEmployee");
                var formData = createEmployee.serialize();

                createEmployee.find( '#first-name-error' ).html( "" );
                createEmployee.find( '#last-name-error' ).html( "" );
                createEmployee.find( '#email-error' ).html( "" );

                $.ajax({
                    url:'{{route("employees.store")}}',
                    type:'POST',
                    data:formData,
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.first_name){
                                createEmployee.find( '#first-name-error' ).html( data.errors.first_name[0] );
                            }
                            if(data.errors.last_name){
                                createEmployee.find( '#last-name-error' ).html( data.errors.last_name[0] );
                            }
                            if(data.errors.email){
                                createEmployee.find( '#email-error' ).html( data.errors.email[0] );
                            }
                        }
                        if(data.success) {
                            swal.fire('Done!','it was successfully Added!', 'success');
                            $("#employee-create-modal").modal("hide");
                            datatable.ajax.reload();
                        }
                    },
                });
            });


        });
    </script>
@stop