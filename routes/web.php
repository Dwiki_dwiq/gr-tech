<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'register' => false, // Register is false
    'reset' => false,  // Password Reset is false
    'verify' => false, // Email Verification Routes is false
]);



Route::middleware(['auth'])->group(function(){

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    
    Route::middleware(['admin'])->group(function(){
        
        Route::resource('companies', App\Http\Controllers\CompaniesController::class);
        Route::get('/getdata/companies', [ App\Http\Controllers\CompaniesController::class, 'get_data_json'])->name('companies.getdata');

        Route::resource('employees', App\Http\Controllers\EmployeesController::class);
        Route::get('/getdata/employees', [ App\Http\Controllers\EmployeesController::class, 'get_data_json'])->name('employees.getdata');

    });

});
